/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.shapinheritance;

/**
 *
 * @author Acer
 */
public class Square extends Rectangle {
    public Square(double side){
        super(side,side);
    }
    @Override
    public void ShowArea(){
        System.out.println("Square side : "+this.width+" Area: "+this.calArea());
    }
}
