/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.shapinheritance;

/**
 *
 * @author Acer
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public Triangle(double base ,double height){
        if(base==0 || height==0){
            System.out.println("Base&Height = 0 ");
            return;
    }
        this.base=base;
    this.height=height;
    }
    @Override
    public double calArea(){
        return this.base*this.height/2;
    }
    @Override
    public void ShowArea(){
        System.out.println("Triangle base : "+this.base+ " Height: "+this.height+" Area = "+this.calArea());
    }
}
